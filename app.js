#!/usr/bin/env node

'use strict';

var program = require('commander');
const path = require('path');
var fs = require('fs');
var cheerio = require('cheerio');
var Handlebars = require('handlebars');
var Entities = require('html-entities').XmlEntities;
var entities = new Entities();
var chokidar = require('chokidar');
var ncp = require('ncp').ncp;

var gulp = require('gulp');
var sass = require('gulp-sass');

sass.compiler = require('node-sass');

var currentDir = process.cwd();

var changes = {

};

var templates = {

};

function splitPage(content) {
    // console.log(content);
    var split = content.split("--[]--");
    // console.log(split);
    var configuration;
    try {
        configuration = JSON.parse(split[0]);
    } catch(err) {
        console.log(err);
    }
	
	return{
        html: split[1],
        config: configuration       
    };
}

function renderPage(file){
    changes[file] = 1;
    fs.readFile(`${currentDir}/src/pages/${file}`, 'utf8',(file_read_err, file_contents) => {
        // console.log('err', file_read_err);
        // console.log('content', file_contents)
        var content = splitPage(file_contents);

        var template = Handlebars.compile(templates[content.config.template].content);
        var product = template({
            "title": content.config.title,
            "content": content.html
        });

        product = entities.decode(product);

        fs.writeFileSync(`${currentDir}/out/${file}`, product, (err) => {
            if (err) console.log('save file error', err);
            console.log(`Built ${file}`);
        });
    });
	changes[file] = 0;
}

function buildScss(){
	gulp.task('sass', () => {
		return gulp.src('./src/css/*.scss').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('./out/css'));
	});
}

function build(){
	fs.readdir(currentDir + '/src/templates/', (templatelist_error, templates_list) => {
        templates = {};
		templates_list.forEach(template => {
			console.log('Loading Template ', template)
			var content = splitPage(fs.readFileSync(`${currentDir}/src/templates/${template}`, 'utf8'));
			templates[content.config["name"]] = {
				content: entities.decode(content.html)
			}
		});

		fs.readdir(currentDir + "/src/pages", (pagelist_error, pages) => {
			pages.forEach(file => {
				console.log('Render Page ', file);
				renderPage(file);
			});
		});
	});

	buildScss();

	ncp(`${currentDir}/src/misc/`, `${currentDir}/out/`, (err) => {
		if (err) console.log;
		console.log('Coppied Extras');
	});

	console.log('Done!');
}

program
    .command('init [initDir]')
    .description('Initialize Directories')
    .action((initDir) => {
        function newDir(name) {
            if(fs.existsSync(name)){
    
            }else {
                fs.mkdirSync(name);
            }
        }
        
        if(!initDir) {
            newDir(currentDir + "/new-site");
            initDir = "new-site";
        }else if(initDir == '.'){
            initDir = "/";
        } else {
            newDir(currentDir + `/${initDir}`);
        }
    
        newDir(currentDir + `/${initDir}/src`);
        newDir(currentDir + `/${initDir}/src/templates`);
        newDir(currentDir + `/${initDir}/src/pages`);
        newDir(currentDir + `/${initDir}/src/data`);
        newDir(currentDir + `/${initDir}/src/css`);
        newDir(currentDir + `/${initDir}/src/misc`);
        newDir(currentDir + `/${initDir}/out`);
        newDir(currentDir + `/${initDir}/out/css`);
    
        fs.open(currentDir + `/${initDir}/project.json`, 'w', (err, file) => {
            if (err) throw err;
        });
    });

program
    .command('build')
    .description('Build Website')
    .action(() => {
        build();
    });

program
    .command('test')
    .description('Test Website')
    .action(() => {
        console.log('Serving Website...');
        var Express = require('express');
        var express = Express();
    
        build();
    
    
        express.get('/*', (req, res) => {
            if(path.extname(req.path) == ".html"){
                var $ = cheerio.load(fs.readFileSync(`${currentDir}/out/${req.path}`));
                $('head').append(`<script type="text/javascript" src="http://livejs.com/live.js"></script>`);
                res.send($.html());
            }else {
                res.sendFile(`${currentDir}/out/${req.path}`);
            }
            
        });
    
        chokidar.watch(`${currentDir}/src`).on('all', (e, path_) => {
            // console.log(path_);
            if(e == "change"){
                function tmp(){
                    try {
                        if(path.extname(path_) == ".scss"){
                            buildScss();
                        }else {
                            setTimeout(build, 1500);
                            // build();
                        }
                    } catch(err) {
                        console.log('render error');
                        tmp();
                    }
                }
                tmp();
            }
        });
    
        var listener = express.listen(8000, () => {
            console.log(`Serving on ${listener.address().port}!`);
        });
    })

program.parse(process.argv);