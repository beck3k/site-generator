#!/usr/bin/env node

'use strict';

var program = require('commander');
const path = require('path');
var fs = require('fs');
var cheerio = require('cheerio');
var Handlebars = require('handlebars');
var Entities = require('html-entities').XmlEntities;
var entities = new Entities();
var chokidar = require('chokidar');
var ncp = require('ncp').ncp;

var gulp = require('gulp');
var sass = require('gulp-sass');

sass.compiler = require('node-sass');

var currentDir = process.cwd();
var initDir;

// program.version('0')

program
	.command('build')
	.description('Build your website')
	.action((cmd) => {
		console.log('Building Website...');
		build();
	});
// .command('test', 'Serving website for testing')
// .command('init', 'Initalize directories')
// .arguments('[initDir]')
// .action((initDirT)=> {
	// initDir = initDirT;
// })
// .option('create')
// .arguments('<type>')
// .action((action) => {
// 	console.log(action);
// })

program
	.command('test')
	.description('')

program.parse(process.argv);

function splitPage(content) {
	var split = content.split("--[]--");
	var configuration = JSON.parse(split[0]);
	return{
		html: split[1],
console.log('Serving Website...');
	var Express = require('express');
	var express = Express();

	build();


	express.get('/*', (req, res) => {
		if(path.extname(req.path) == ".html"){
			var $ = cheerio.load(fs.readFileSync(`${currentDir}/out/${req.path}`));
			$('head').append(`<script type="text/javascript" src="http://livejs.com/live.js"></script>`);
			res.send($.html());
		}else {
			res.sendFile(`${currentDir}/out/${req.path}`);
		}
		
	});

	chokidar.watch(currentDir).on('all', (e, path_) => {
		if(e == "change"){
			function tmp(){
				try {
					if(path.extname(path_) == ".scss"){
						buildScss();
					}else {
						build();
					}
				} catch(err) {
					console.log('render error');
					tmp();
				}
			}
			tmp();
		}
	});

	var listener = express.listen(8000, () => {
		console.log(`Serving on ${listener.address().port}!`);
	});
console.log('Serving Website...');
	var Express = require('express');
	var express = Express();

	build();


	express.get('/*', (req, res) => {
		if(path.extname(req.path) == ".html"){
			var $ = cheerio.load(fs.readFileSync(`${currentDir}/out/${req.path}`));
			$('head').append(`<script type="text/javascript" src="http://livejs.com/live.js"></script>`);
			res.send($.html());
		}else {
			res.sendFile(`${currentDir}/out/${req.path}`);
		}
		
	});

	chokidar.watch(currentDir).on('all', (e, path_) => {
		if(e == "change"){
			function tmp(){
				try {
					if(path.extname(path_) == ".scss"){
						buildScss();
					}else {
						build();
					}
				} catch(err) {
					console.log('render error');
					tmp();
				}
			}
			tmp();
		}
	});

	var listener = express.listen(8000, () => {
		console.log(`Serving on ${listener.address().port}!`);
	});
}console.log('Serving Website...');
	var Express = require('express');
	var express = Express();

	build();


	express.get('/*', (req, res) => {
		if(path.extname(req.path) == ".html"){
			var $ = cheerio.load(fs.readFileSync(`${currentDir}/out/${req.path}`));
			$('head').append(`<script type="text/javascript" src="http://livejs.com/live.js"></script>`);
			res.send($.html());
		}else {
			res.sendFile(`${currentDir}/out/${req.path}`);
		}
		
	});

	chokidar.watch(currentDir).on('all', (e, path_) => {
		if(e == "change"){
			function tmp(){
				try {
					if(path.extname(path_) == ".scss"){
						buildScss();
					}else {
						build();
					}
				} catch(err) {
					console.log('render error');
					tmp();
				}
			}
			tmp();
		}
	});

	var listener = express.listen(8000, () => {
		console.log(`Serving on ${listener.address().port}!`);
	});

var changes = {

};

var templates = {

};

function renderPage(file){
	changes[file] = 1;
	var content = splitPage(fs.readFileSync(`${currentDir}/src/pages/${file}`, 'utf8'));

	var template = Handlebars.compile(templates[content.config.template].content);
	var product = template({
		"title": content.config.title,
		"content": content.html
	});

	product = entities.decode(product);

	fs.writeFileSync(`${currentDir}/out/${file}`, product, (err) => {
		if (err) console.log('save file error', err);
		console.log(`Built ${file}`);
	});
	changes[file] = 0;
}

function buildScss(){
	gulp.task('sass', () => {
		return gulp.src('./src/css/*.scss').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('./out/css'));
	});
}

function build(){
	fs.readdir(currentDir + '/src/templates/', (templatelist_error, templates_list) => {
		templates_list.forEach(template => {
			console.log('Loading Template ', template)
			var content = splitPage(fs.readFileSync(`${currentDir}/src/templates/${template}`, 'utf8'));
			templates[content.config["name"]] = {
				content: entities.decode(content.html)
			}
		});

		fs.readdir(currentDir + "/src/pages", (pagelist_error, pages) => {
			pages.forEach(file => {
				console.log('Render Page ', file);
				renderPage(file);
			});
		});
	});

	buildScss();

	ncp(`${currentDir}/src/misc/`, `${currentDir}/out/`, (err) => {
		if (err) console.log;
		console.log('Coppied Extras');
	});

	console.log('Done!');
}

if(program.build) {
	
} else if(program.test){
	

	
} else if(program.init) {
	

	
}
